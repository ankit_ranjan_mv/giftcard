package com.giftcard.models.flipkart;

import lombok.Data;

@Data
public class CreateTransactionResponse {
    private String statusCode; //TODO can be made as an enum
    private String statusMessage;
    private String transactionId;
    private String transactionStatus; //TODO can be made as an enum
}
