package com.giftcard.models.mv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.giftcard.constants.enums.CouponActivationStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@Table(name = "coupon_voucher")
@JsonIgnoreProperties(ignoreUnknown = true)
@EntityListeners(AuditingEntityListener.class)
public class CouponVoucher {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "voucher_id")
    private String voucherId;

    @Column(name = "gift_coupon_id")
    private String giftCouponId;

    @Column(name = "voucher_code")
    private String voucherCode;

    @Column(name = "voucher_url")
    private String voucherURL;

    @Column(name = "voucher_pin")
    private String voucherPin;

    @Column(name = "voucher_status")
    private CouponActivationStatus voucherStatus;

    @Column
    private int amount;

    @Column(name = "date_created")
    @CreationTimestamp
    private Timestamp dateCreated;

    @Column(name = "date_modified")
    @UpdateTimestamp
    private Timestamp dateModified;
}
