package com.giftcard.models.mv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.giftcard.constants.enums.CouponDeliveryStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@Table(name = "coupon_order")
@JsonIgnoreProperties(ignoreUnknown = true)
@EntityListeners(AuditingEntityListener.class)
public class CouponOrder {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column
    private String id;

    @Column(name = "loan_application_id")
    private String loanApplicationId;

    @Enumerated(EnumType.STRING)
    private CouponDeliveryStatus couponDeliveryStatus;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "coupon_amount")
    private int couponAmount;

    @Column(name = "bank_partner_id")
    private int bankPartnerId;

    @Column(name = "merchant_id")
    private int merchantId;

    @Column(name = "coupon_provider_id")
    private int couponProviderId;

    @Column(name = "date_created")
    @CreationTimestamp
    private Timestamp dateCreated;

    @Column(name = "date_modified")
    @UpdateTimestamp
    private Timestamp dateModified;
}
