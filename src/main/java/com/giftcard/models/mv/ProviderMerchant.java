package com.giftcard.models.mv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "provider_merchant")
@EntityListeners(AuditingEntityListener.class)
public class ProviderMerchant {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private int id;

    @Column(name = "provider_id")
    private String providerId;

    @Column(name = "merchant_id")
    private String merchantId;

    @Column
    private String config;
}
