package com.giftcard.models.mv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.giftcard.constants.enums.Category;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "product")
@EntityListeners(AuditingEntityListener.class)
public class Product {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private int id;

    @Column(name = "product_id")
    private String productId;

    @Column
    private String name;

    @Column(name = "category_id")
    private String categoryId;

    @Column(name = "category_name")
    private String categoryName;

    @Column
    private String description;

    @Column(name = "minimum_price")
    private Double minimumPrice;

    @Column(name = "maximum_price")
    private Double maximumPrice;

    @Column(name = "custom_denomination")
    private String customDenominations;

    @Column
    private Double intervals;

    @Column(name = "user_handling_charge")
    private Double userHandlingCharge;

    @Enumerated(EnumType.STRING)
    private Category category;

    @Column(name = "provider_id")
    private String providerId;
}
