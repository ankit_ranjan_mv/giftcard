package com.giftcard.models.qc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentMethod {
    private String value;
    private String label;
    private String info;
}
