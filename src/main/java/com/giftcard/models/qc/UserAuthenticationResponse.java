package com.giftcard.models.qc;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserAuthenticationResponse {
    private boolean success;
    private String verifier;
    private String message;
}
