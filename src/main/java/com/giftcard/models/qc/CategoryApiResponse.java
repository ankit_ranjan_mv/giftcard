package com.giftcard.models.qc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryApiResponse {
    private RootCategory root_category;
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class RootCategory {

        @JsonProperty("category_id")
        private String categoryId;

        @JsonProperty("category_name")
        private String categoryName;

        @JsonProperty("category_image")
        private String categoryImage;
    }
}