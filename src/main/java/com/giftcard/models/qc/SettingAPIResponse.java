package com.giftcard.models.qc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SettingAPIResponse {
    private boolean success;
    private Integer store_id;
    private Map<String, String> config;
    private List<PaymentMethod> payment_method;
    private String terms_and_conditions;
    private String powered_by_url;
    private String logo_url;
    private String base_url;
}
