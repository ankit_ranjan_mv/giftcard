package com.giftcard.models.qc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryDetailsResponse {
    private boolean success;
    private String id;
    private Embedded _embedded;
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Embedded {
        private List<QCProduct> product;
    }
}
