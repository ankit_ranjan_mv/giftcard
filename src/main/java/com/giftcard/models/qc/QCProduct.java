package com.giftcard.models.qc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QCProduct {
    private String id;
    private String sku;
    private String brand_id;
    private CouponImages images;
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class CouponImages {
        private String image;
        private String small_image;
        private String thumbnail;
        private String mobile;
        private String base_image;
    }
    private String price_type;
    private String navigation_apicall;
    private String navigation_urlpath;
    private String min_custom_price;
    private String max_custom_price;
    private String custom_denominations;
}
