package com.giftcard.constants;

public class Constants {
    public static final String FLIPKART_DOMAIN_URL = "flipkart.domain.url"; //TODO to be set into db
    public static final String FLIPKART_CREATE_TRANSACTION_URL = "/gcms/api/1.0/transaction";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
}
