package com.giftcard.constants.enums;

public enum CouponActivationStatus {
    ACTIVE, INACTIVE, CANCELLED, EXPIRED, REDEEMED
}
