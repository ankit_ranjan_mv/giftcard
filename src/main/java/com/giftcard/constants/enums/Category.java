package com.giftcard.constants.enums;

public enum Category {
    DIGITAL("1"), PHYSICAL("2");

    String value;

    Category(String value){
        this.value = value;
    }

    public static Category getById(String value){
        for (Category category : Category.values()){
            if (value.equals(category.value))
                return category;
        }
        return null;
    }

    public String getValue(){
        return this.value;
    }
}
