package com.giftcard.constants.enums;

public enum CouponDeliveryStatus {
    PENDING, INITIATED, RECEIVED, FAILED
}
