package com.giftcard.service;

public interface GiftCardService {

    void authorizeRequest();

    void createGiftCard();

    void viewGiftCard();

    void activateGiftCard();

    void deactivateGiftCard();

    void cancelGiftCard();
}
