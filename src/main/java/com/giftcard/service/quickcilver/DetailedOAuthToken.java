package com.giftcard.service.quickcilver;

import org.springframework.social.oauth1.OAuthToken;

public class DetailedOAuthToken extends OAuthToken {
    /**
     * Create a new OAuth token with a token value and secret.
     *
     * @param value  the token value
     * @param secret the token secret
     */

    private String message;
    public DetailedOAuthToken(String value, String secret, String message) {
        super(value, secret);
        this.message = message;
    }
}
