package com.giftcard.service.quickcilver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.giftcard.constants.Constants;
import com.giftcard.models.qc.*;
import com.giftcard.service.GiftCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.social.connect.support.OAuth1ConnectionFactory;
import org.springframework.social.oauth1.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Component("qcGiftCardService")
public class QCGiftCardService implements GiftCardService {

    private static final Logger log = LoggerFactory.getLogger(QCGiftCardService.class);

    private static final String QUICK_CILVER_PROVIDER_ID = "qcilver";
    private static final String BASE_URL = "https://sandbox.woohoo.in/";
    private static final String CONSUMER_KEY = "8af50260ae5444bdc34665c2b6e6daa9";
    private static final String CONSUMER_SECRET = "93c1d8f362749dd1fe0a819ae8b5de95";
    private static final String REQUEST_TOKEN_URL = BASE_URL + "oauth/initiate?oauth_callback=oob";
    private static final String AUTHORIZATION_URL = BASE_URL + "oauth/authorize/customerVerifier";
    private static final String QC_MV_USER_ID = "moneyviewamazonapisandbox@woohoo.in";
    private static final String QC_MV_PASS = "moneyviewamazonapisandbox@1234";
    private static final String ACCESS_TOKEN_URL = BASE_URL + "oauth/token";
    private static final String REST_END_POINT_URL = BASE_URL + "rest";
    private static final String SETTINGS_API_URL = "/settings";
    private static final String CATEGORY_API_URL = "/category";
    private static final String AMAZON_GC_PRODUCT_ID = "135";
    private static final String PRODUCT_API_URL = "/product";
    private static final int retry_num =3;

    private RestTemplate restTemplate =  new RestTemplate();
    private ObjectMapper objectMapper =  new ObjectMapper();

    private String settingsUrl;
    private String categoryUrl;
    private String productUrl;
    private Map<String, List<String>> parameters = new HashMap<>();


    @PostConstruct
    public void init(){
        settingsUrl = REST_END_POINT_URL + SETTINGS_API_URL;
        categoryUrl = REST_END_POINT_URL + CATEGORY_API_URL;
        productUrl = REST_END_POINT_URL + PRODUCT_API_URL;
        parameters.put(Constants.USERNAME, Collections.singletonList(QC_MV_USER_ID));
        parameters.put(Constants.PASSWORD, Collections.singletonList(QC_MV_PASS));
    }

    private OAuth1ConnectionFactory connectionFactory = new GenericOAuth1ConnectionFactory(
            QUICK_CILVER_PROVIDER_ID,
            CONSUMER_KEY,
            CONSUMER_SECRET,
            REQUEST_TOKEN_URL,
            AUTHORIZATION_URL,
            ACCESS_TOKEN_URL,
            OAuth1Version.CORE_10_REVISION_A,
            null
            );


    @Override
    public void authorizeRequest() {
        try {
            OAuthBinding oAuthBinding = authenticateUser();
            //We are authenticated and ready to call provider APIs with below oauthBinding. Move this to a factory
            AtomicReference<ResponseEntity<SettingAPIResponse>> responseEntitySettingsApi = new AtomicReference<>(oAuthBinding.getRestTemplate().getForEntity(settingsUrl, SettingAPIResponse.class));
            SettingAPIResponse settingAPIResponse = responseEntitySettingsApi.get().getBody();
            ResponseEntity<CategoryApiResponse> responseEntityCategoryApi = oAuthBinding.getRestTemplate().getForEntity(categoryUrl, CategoryApiResponse.class);
            if (responseEntityCategoryApi.getStatusCodeValue() == 200){
                CategoryApiResponse categoryApiResponse = responseEntityCategoryApi.getBody();
                String categoryId = categoryApiResponse.getRoot_category().getCategoryId();
                ResponseEntity<CategoryDetailsResponse> categoryDetailsResponseResponseEntity = oAuthBinding.getRestTemplate().getForEntity(categoryUrl + "/" + categoryId, CategoryDetailsResponse.class);
                if (categoryDetailsResponseResponseEntity!=null && categoryDetailsResponseResponseEntity.getStatusCodeValue() == 200 && categoryDetailsResponseResponseEntity.getBody()!=null){
                    CategoryDetailsResponse categoryDetailsResponse = categoryDetailsResponseResponseEntity.getBody();
                    String sku = categoryDetailsResponse.get_embedded().getProduct().stream().filter(a -> a.getId().equals(AMAZON_GC_PRODUCT_ID)).findAny().map(QCProduct::getSku).get();
                    ResponseEntity<QCProduct> productResponseEntity = oAuthBinding.getRestTemplate().getForEntity(productUrl + "/" + sku, QCProduct.class);
                    if (productResponseEntity!=null && productResponseEntity.getStatusCodeValue() == 200){
                        QCProduct qcProduct = productResponseEntity.getBody();
                    }
                }
            }
        } catch (RestClientException r){
            log.error("Rest client exception occured "+r.getMessage());
        } catch (InvalidTokenException d){
            log.error("Invalid token exception occured "+d.getMessage());
        } catch(Exception e){
            log.error("response is null "+e.getMessage());
        }
    }

    private Map<String, List<String>> createParams() {
        Map<String, List<String>> parameter = new HashMap<>();
        return null;

    }

    private MultiValueMap<String, String> getAdditionalParams() {
        MultiValueMap<String, String> additionalParams = new LinkedMultiValueMap<>();
        additionalParams.set(Constants.USERNAME, QC_MV_USER_ID);
        additionalParams.set(Constants.PASSWORD, QC_MV_PASS);
        return additionalParams;
    }

    @Override
    public void createGiftCard() {

    }

    @Override
    public void viewGiftCard() {

    }

    @Override
    public void activateGiftCard() {

    }

    @Override
    public void deactivateGiftCard() {

    }

    @Override
    public void cancelGiftCard() {

    }

    private void throwInvalidTokenException(String message) {
        throw new InvalidTokenException(message);
    }

    public OAuthToken fetchRequestToken(String callbackUrl, OAuth1Operations oAuth1Operations, int retry){
        OAuthToken oAuthRequestToken = null;
        if (retry==retry_num){
            return oAuthRequestToken;
        }
        try {
            oAuthRequestToken = oAuth1Operations.fetchRequestToken(callbackUrl, null);
        } catch(RestClientException r){
            log.error("Error while fetching request token with callback uri oob "+ r.getMessage());
            oAuthRequestToken = fetchRequestToken(callbackUrl,oAuth1Operations, retry+1);
        }
        return oAuthRequestToken;
    }

    public ResponseEntity<String> getForEntity(URI oauthUri, int retry){
        ResponseEntity<String> response = null;
        if (retry==retry_num){
            return response;
        }
        try {
            response = restTemplate.getForEntity(oauthUri, String.class);
        } catch(RestClientException r){
            log.error("Exception Occured in getting response with uri "+ oauthUri.getPath()+ " retry "+ retry);
            response = getForEntity(oauthUri, retry+1);
        }
        return response;
    }

    public OAuthToken exchangeForAccessToken(AuthorizedRequestToken requestToken, OAuth1Operations oAuth1Operations, int retry){
        OAuthToken oAuthVerifiedToken = null;
        if (retry==retry_num){
            return oAuthVerifiedToken;
        }
        try {
            oAuthVerifiedToken = oAuth1Operations.exchangeForAccessToken(requestToken, getAdditionalParams());
        } catch(RestClientException r){
            log.error("invalid verifier for exchangeForAccessToken"+ " retry "+ retry);
            oAuthVerifiedToken = exchangeForAccessToken(requestToken, oAuth1Operations, retry+1);
        }
        return oAuthVerifiedToken;

    }

    public OAuthBinding authenticateUser() {
        OAuthBinding oAuthBinding = null;
        UserAuthenticationResponse userAuthenticationResponse = null;
        OAuth1Operations oAuth1Operations = connectionFactory.getOAuthOperations();
        OAuthToken oAuthRequestToken = null;
        oAuthRequestToken = fetchRequestToken("oob1", oAuth1Operations, 0);
        if (oAuthRequestToken == null || oAuthRequestToken.getValue() == null || oAuthRequestToken.getSecret() == null) {
            throwInvalidTokenException("message with oAuthRequestToken callback uri oob");
        }

            /*Below encodes @ to %40. need to find a way to get rid of this
            ResponseEntity<String> response = restTemplate.getForEntity(oAuth1Operations.buildAuthorizeUrl(oAuthRequestToken.getValue(),new OAuth1Parameters(parameters)), String.class);*/
        ResponseEntity<String> response = null;
        URI oauthUri = UriComponentsBuilder.fromHttpUrl(AUTHORIZATION_URL + "?oauth_token=" + oAuthRequestToken.getValue() + "&" + Constants.USERNAME + "=" + QC_MV_USER_ID + "&" + Constants.PASSWORD + "=" + QC_MV_PASS).build().toUri();
        response = getForEntity(oauthUri, 0);
        if (response == null || response.getBody() == null) {
            throwInvalidTokenException("Error in getForEntity with uri " + oauthUri + " and oauth Token " + oAuthRequestToken.getValue());
        }

        try {
            userAuthenticationResponse = objectMapper.readValue(response.getBody(), UserAuthenticationResponse.class);
        } catch (IOException e) {
            log.error("Unable to parse the Json userAuthenticationResponse " + response.getBody() + userAuthenticationResponse);
        }
        if (userAuthenticationResponse != null) {
            OAuthToken oAuthVerifiedToken = null;
            if (!userAuthenticationResponse.isSuccess() || userAuthenticationResponse.getVerifier() == null) {
                throwInvalidTokenException("Could not authenticate userAuthenticationResponse " + userAuthenticationResponse);
            } else {
                AuthorizedRequestToken requestToken = new AuthorizedRequestToken(oAuthRequestToken, userAuthenticationResponse.getVerifier());
                oAuthVerifiedToken = exchangeForAccessToken(requestToken, oAuth1Operations, 0);
                if (oAuthVerifiedToken == null || oAuthVerifiedToken.getValue() == null || oAuthVerifiedToken.getSecret() == null) {
                    throwInvalidTokenException("Error in fetching oAuthVerifiedToken");
                }
                oAuthBinding = new OAuthBinding(CONSUMER_KEY, CONSUMER_SECRET, oAuthVerifiedToken.getValue(), oAuthVerifiedToken.getSecret());
            }

        }

        return oAuthBinding;
    }

}
