package com.giftcard.service.quickcilver;

import org.springframework.social.oauth1.AbstractOAuth1ApiBinding;

public class OAuthBinding extends AbstractOAuth1ApiBinding {

    public OAuthBinding(String consumerKey, String consumerSecret, String accessToken, String accessTokenSecret) {
        super(consumerKey, consumerSecret, accessToken, accessTokenSecret);
    }
}
