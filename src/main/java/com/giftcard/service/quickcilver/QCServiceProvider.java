package com.giftcard.service.quickcilver;

import org.springframework.social.oauth1.AbstractOAuth1ServiceProvider;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Template;

public class QCServiceProvider extends AbstractOAuth1ServiceProvider {

    private static final String QUICK_CILVER_PROVIDER_ID = "qcilver";
    private static final String BASE_URL = "https://sandbox.woohoo.in/";
    private static final String CONSUMER_KEY = "8af50260ae5444bdc34665c2b6e6daa9";
    private static final String CONSUMER_SECRET = "93c1d8f362749dd1fe0a819ae8b5de95";
    private static final String REQUEST_TOKEN_URL = BASE_URL + "oauth/initiate?oauth_callback=oob";
    private static final String AUTHORIZATION_URL = BASE_URL + "oauth/authorize/customerVerifier";
    private static final String QC_MV_USER_ID = "moneyviewamazonapisandbox@woohoo.in";
    private static final String QC_MV_PASS = "moneyviewamazonapisandbox@1234";
    private static final String ACCESS_TOKEN_URL = BASE_URL + "oauth/token";
    private static final String REST_END_POINT_URL = BASE_URL + "rest";
    private static final String SETTINGS_API_URL = "/settings";
    /**
     * Creates a OAuth1ServiceProvider.
     *
     * @param consumerKey      the consumer (or client) key assigned to the application by the provider.
     * @param consumerSecret   the consumer (or client) secret assigned to the application by the provider.
     * @param oauth1Operations the template that allows the OAuth1-based authorization flow to be conducted with the provider.
     */
    public QCServiceProvider(String consumerKey, String consumerSecret, OAuth1Operations oauth1Operations) {
        super(consumerKey, consumerSecret, new OAuth1Template(CONSUMER_KEY, CONSUMER_SECRET, REQUEST_TOKEN_URL, AUTHORIZATION_URL, ACCESS_TOKEN_URL));
    }

    @Override
    public Object getApi(String accessToken, String secret) {
        return null;
    }
}
