package com.giftcard.service.quickcilver;

import com.giftcard.models.qc.SettingAPIResponse;

public interface QCApi {
    SettingAPIResponse settings();
}
