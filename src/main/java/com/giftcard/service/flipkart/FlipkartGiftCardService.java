package com.giftcard.service.flipkart;

import com.giftcard.service.GiftCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Template;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component("flipkartGiftCardService")
public class FlipkartGiftCardService implements GiftCardService {

    private RestTemplate restTemplate = new RestTemplate();

    private static final Logger log = LoggerFactory.getLogger(FlipkartGiftCardService.class);

    @Override
    public void authorizeRequest() {
    }

    @Override
    public void createGiftCard() {

    }

    @Override
    public void viewGiftCard() {

    }

    @Override
    public void activateGiftCard() {

    }

    @Override
    public void deactivateGiftCard() {

    }

    @Override
    public void cancelGiftCard() {

    }
}
