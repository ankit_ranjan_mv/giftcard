package com.giftcard.repository;

import com.giftcard.models.mv.CouponVoucher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponVoucherRepository extends JpaRepository<CouponVoucher, String> {
}
