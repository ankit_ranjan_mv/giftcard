package com.giftcard.repository;

import com.giftcard.models.mv.CouponOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CouponOrderRepository extends JpaRepository<CouponOrder, String> {

    CouponOrder findByLoanApplicationId(String loanAppId);
}
