package com.giftcard.controller;

import com.giftcard.repository.CouponOrderRepository;
import com.giftcard.service.GiftCardService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "Controller")
@RequestMapping(value = "")
public class Controller {

    @Autowired
    CouponOrderRepository repository;

    @Autowired
    GiftCardService qcGiftCardService;

    @ApiOperation(value = "test qc authorization")
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void getDetails(){
        qcGiftCardService.authorizeRequest();
    }
}
